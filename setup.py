#!/usr/bin/env python3.3

### Setup IAS
from iaslib.core import sysias
from iasconf import setup

sysias.pinfo("Installation de I.A.S v2")
sysias.clean()

### Maj Gentoo
sysias.runcmd("Mise a jour de l'arbre portage : ", "emergesync", "emerge --sync")
sysias.runcmd("Mise a jour de portage : ", "portage", "emerge --oneshot portage")

### Installation de l'environnement emerge
sysias.pinfo("Installation de l'environnement portage ...")
sysias.copydir(setup.gentoodir, setup.portagedir)

### MaJ make.conf
sysias.runcmd("Mise a jour make.conf : ", "makeconf", "sed -i 's/USE=\"/USE=\"python vim-syntax /g' "+setup.portagedir+"/make.conf")

# Ready, Set, Go
sysias.pinfo("Installation des packages : ")
for pkg in setup.pkglist:
    sysias.installsoft(pkg)

# Finish install
sysias.runcmd("Demarrage de Nginx : ", "nginxstart", "/etc/init.d/nginx start")
sysias.runcmd("Demarrage de php-fpm : ", "phpstart", "/etc/init.d/php-fpm start")
sysias.runcmd("Redemarrage de syslog-ng : ", "syslogngstart", "/etc/init.d/syslog-ng restart")
sysias.runcmd("Redemarrage de sshd : ", "sshdstart", "/etc/init.d/sshd restart")
sysias.runcmd("Demarrage de shorewall : ", "shorewallstart", "/etc/init.d/shorewall start")
sysias.runcmd("Demarrage de varnishd : ", "varnishdstart", "/etc/init.d/varnishd start")
sysias.runcmd("Demarrage de fail2ban : ", "fail2banstart", "/etc/init.d/fail2ban start")

sysias.pwarn("You have to reboot your system now !")