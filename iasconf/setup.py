""" Config File for ias-tools """
"""
    this file contains all config
"""

### IAS var
debug = 0
logdir="logs/"
gentoodir="iasconf/gentoo"
confpkgdir="iasconf/softs"
pkglist=["vim","syslog-ng","logrotate","iptables","shorewall","fail2ban","mysql","libnss-mysql","php","nginx","uwsgi","varnish","vsftpd"]
#pkglist=["libnss-mysql","php","nginx","uwsgi","varnish","vsftpd"]
iasdb = "iasAdm"
iasuser = "ias"
iaspwd = "!@5p@ssw0rd"
nssuser = "nssmysql"
nsspass = "dPbH65d2!"

### Gentoo var
confdir = '/etc'
portagedir = confdir + '/portage'


# NGINX config section
nginx_user = 'nginx'
nginx_doc_root = '/home/http'
nginx_enable_path = '/etc/nginx/sites-enabled'
nginx_available_path = '/etc/nginx/sites-available'
nginx_security_file = '/etc/nginx/security'
nginx_fastcgi_file = '/etc/nginx/fastcgi_params'
nginx_web_dir = 'www/htdocs'
nginx_index_file = 'index.html'

php_index_file = 'index.php'
php_socks_file = 'socks/php.socket'

nginx_tpl = """server {{
    listen 127.0.0.1:80;
    server_name {vhost};
    root {docroot}/{domain}/{host}/{webdir};
    index {indexfile};
    include {security};
    
    access_log syslog:warn main;
    error_log syslog:warn;

    location ~ /\.ht {{
        deny all;
    }}

    location /favicon.ico {{
        access_log off;
        log_not_found off;
        expires 1d;
    }}
"""

nginx_php_tpl = """
    location ~ \.php$ {{
        try_files $uri =404;
        include {fastcgi};
        fastcgi_pass unix:{docroot}/{domain}/{host}/{socketfile};
        fastcgi_index {indexfile};
    }}
"""

nginx_close_tpl = """
}}
"""