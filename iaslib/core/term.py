""" Terminal functions """

""" Return a string with red ASCII code """
def red(string):
    return '\x1b[1;31m%s\x1b[0m' % string

""" Return a string with green ASCII code """
def green(string):
    return '\x1b[1;32m%s\x1b[0m' % string

""" Return a string with yellow ASCII code """
def yellow(string):
    return '\x1b[1;33m%s\x1b[0m' % string

""" Return a string with white ASCII code """
def white(string):
    return '\x1b[1;37m%s\x1b[0m' % string

""" Return a string with ASCII 'moving to column 60' code """
def side(string):
    return '\x1b[60G [ %s ]\x1b[0m' % string

