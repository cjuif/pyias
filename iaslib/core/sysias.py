""" OS lib file. Abstract Easy """

from iaslib.core import term
from iasconf import setup

from subprocess import Popen, STDOUT
from sys import exit
from os import path, makedirs, listdir, chmod
from shutil import copy2, rmtree
from socket import gethostname, socket, AF_INET, SOCK_DGRAM
import crypt
import getpass



def sys_exit(msg):
    print(term.red("ERROR: " + msg), flush=True)
    exit(1)

def pinfo(msg):
    print(term.yellow(msg), flush=True)

def pwarn(msg):
    print(term.red(msg), flush=True)
    
def pmsg(msg):
    print(term.white(msg),end="", flush=True)

def pstatus(msg):
    print(term.side(msg), flush=True)
    
def clean():
    rmtree(setup.logdir)
    mdir(setup.logdir)
    
def runcmd(title, fname, cmd):
    pmsg(title)
    with open(setup.logdir+fname+".log", "wb") as log:
        p = Popen(cmd, shell=True, bufsize=0, stdout=log, stderr=STDOUT)
    if p.wait() != 0:
        pstatus(term.red("FAIL"))
        sys_exit(cmd+" FAILED ")
    else:
        pstatus(term.green("OK"))

def installsoft(pkg):
    runcmd("   - Installation de "+pkg+ " : ", pkg, "emerge "+pkg)
    if isdir(path.join(setup.confpkgdir,pkg)):
        pmsg("   - Configuration de "+pkg+" : ")
        copydir(setup.confpkgdir+'/'+pkg, '/etc', "conf")
        pstatus(term.green("OK"))
    if path.isfile(path.join(setup.confpkgdir,pkg,"conf")):
        with open(path.join(setup.confpkgdir,pkg,"conf")) as f:
            code = compile(f.read(), "conf", 'exec')
            exec(code)

def mdir(directory):
    if not isdir(directory):
        makedirs(directory)
        return True
    sys_exit(directory+" already exist !")
    
def isdir(directory):
    if path.exists(directory):
        return True
    return False

def copydir(src, dst, ignore=None):
    names = listdir(src)
    for name in names:
        srcname = path.join(src,name)
        dstname = path.join(dst,name)
        if path.isdir(srcname):
            if not path.isdir(dstname):
                makedirs(dstname)
            copydir(srcname, dstname)
        else:
            if not srcname == ignore:
                copy2(srcname,dstname)

def get_password(msg=None, crypto=False):
    if msg == None:
        pprompt = lambda: (getpass.getpass(), getpass.getpass('   - Retype password : '))
    else:
        pprompt = lambda: (getpass.getpass(term.white(msg)), getpass.getpass(term.white('   - Retype password : ')))
    p1, p2 = pprompt()
    while p1 != p2:
        print('Passwords do not match. Try again')
        p1, p2 = pprompt()
    if crypto == True:
        p1 = crypt.crypt(p1)
    return p1

def copyfile(src, dst):
    filename = path.basename(src)
    dstname = path.join(dst,filename)
    copy2(src, dstname)

def get_netip():
    s = socket(AF_INET, SOCK_DGRAM)
    s.connect(("8.8.8.8",80))
    ip = s.getsockname()[0]
    s.close()
    return ip