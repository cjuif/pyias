#!/usr/bin/env python3.3

""" Ias Tools : mkvhost """

from os import getcwd
from sys import path, argv

debug = True
conf_dir = '/etc/mktools'

def init_pkg():
    if debug:
        conf_dir = getcwd()
    path.append(conf_dir)

from iaslib.argsparse import parse

if __name__ == '__main__':
    parse(argv)


