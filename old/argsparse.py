""" Parsing Command Line argument """
from sys import exit
import argparse

from iaslib import core

def parse(args):
    p = argparse.ArgumentParser(prog='mkvhost')
    sp = p.add_subparsers()

    # new command
    sp_new = sp.add_parser('new', help='new --help')
    sp_new.add_argument('domain', help='Ex: vudunet.net')
    sp_new.add_argument('host', help='Ex: www')
    sp_new.add_argument('user', help='Ex: vudunet')
    sp_new.add_argument('engine', choices={'none','php','python'}, metavar='engine', help='none|php|python')
    sp_new.set_defaults(func=core.mkvhost)

    # delete command
    sp_del = sp.add_parser('delete', help='delete --help')
    sp_del.add_argument('domain', help='Ex: vudunet.net')
    sp_del.add_argument('host', help='Ex: www')
    sp_del.set_defaults(func=core.delvhost)

    # list command
    sp_list = sp.add_parser('list', help='list --help')
    sp_list.add_argument('status', choices={'all','enable','disable'}, metavar='status', help='all|enable|disable')
    sp_list.add_argument('-p', required=False, help='filter result with pattern')
    sp_list.set_defaults(func=core.listvhost)

    # enable command
    sp_enbl = sp.add_parser('enable', help='enable --help')
    sp_enbl.add_argument('vhost', help='Ex: www.vudunet.net')
    sp_enbl.set_defaults(func=core.enablevhost)

    # disable command
    sp_dsbl = sp.add_parser('disable', help='disable --help')
    sp_dsbl.add_argument('vhost', help='Ex: www.vudunet.net')
    sp_dsbl.set_defaults(func=core.disablevhost)
        
    args = p.parse_args()
    try:
        getattr(args, "func")
    except AttributeError:
        p.print_help()
        exit(0)

    args.func(args)
