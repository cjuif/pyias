#!/usr/bin/env python3.3

import sys
import argparse

# List vhost
import glob
from os import walk
from os import path
from os import link
from os import unlink
from os import getcwd

# login()
import crypt
import getpass

# Declaring global var
debug = True
conf_dir = '/etc/mktools'

def init_pkg():
    if debug:
        conf_dir = getcwd()
    sys.path.append(conf_dir)

# Declaring global var nginx
nginx_doc_root = '/home/http'
nginx_enable_path = '/etc/nginx/sites-enabled'
nginx_available_path = '/etc/nginx/sites-available'

# Declaring text formatting function
def red(str):
    return '\x1b[1;31m%s\x1b[0m' % str

def green(str):
    return '\x1b[1;32m%s\x1b[0m' % str

def white(str):
    return '\x1b[1;37m%s\x1b[0m' % str

def side(str):
    return '\x1b[60G [ %s ]\x1b[0m' %str

# Declaring low level function
def get_path_enable(vhost):
    return path.join(nginx_enable_path, vhost)

def get_path_available(vhost):
    return path.join(nginx_available_path, vhost)

def is_enable(vhost):
    if path.exists(get_path_enable(vhost)):
        return True
    return False

def print_status(vhost):
    if is_enable(vhost):
        print('%s %s' % (white(vhost),side(green('enable'))))
    else:
        print('%s %s' % (white(vhost),side(red('disable'))))


def get_password(crypto=False):
    pprompt = lambda: (getpass.getpass(), getpass.getpass('Retype password: '))

    p1, p2 = pprompt()
    while p1 != p2:
        print('Passwords do not match. Try again')
        p1, p2 = pprompt()
    if crypto == True
        p1 = crypt.crypt(p1)

    return p1

# Create vhost
def mkvhost(args):
    # Verification des entrees (domain host user
    # Creation Rep /home/http/domain/host/{www/htdocs,socks,tmp}
    # Prompt du password
    # Creation user insert sql
    # Creation fichier nginx

    vhost = args.host + '.' + args.domain
    domain = args.domain
    host = args.host
    engine = args.engine

    nginx_tpl = """server {{
    listen 127.0.0.1:80;
    server_name {vhost};
    root {docroot}/{domain}/{host}/www/htdocs;
    index index.php;
    include /etc/nginx/security;
    
    access_log syslog:warn main;
    error_log syslog:warn;

    location ~ /\.ht {{
            deny all;
    }}

    location /favicon.ico {{
            access_log off;
            log_not_found off;
            expires 1d;
    }}

    location ~ \.php$ {{
            try_files $uri =404;
            include /etc/nginx/fastcgi_params;
            fastcgi_pass unix:/home/http/{domain}/{host}/socks/php.socket;
            fastcgi_index index.php;
    }}
}}
"""

    context = {"vhost":vhost,"docroot":setup.nginx_doc_root,"domain":domain,"host":host }
    with open('/etc/nginx/sites-available/'+vhost, 'w') as vhostfile:
        vhostfile.write(nginx_tpl.format(**context))

    log = get_password()
    print(log)

    fpm_tpl = """[{vhost}]

listen = /home/http/{domain}/{host}/socks/php.socket
listen.backlog = -1
listen.owner = nginx
listen.group = nginx
listen.mode = 0660

user = nginx
group = nginx

pm = dynamic
pm.max_children = 75
pm.start_servers = 10
pm.min_spare_servers = 5
pm.max_spare_servers = 20
pm.max_requests = 500

; Pass environment variables
env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin
env[TMP] = /home/http/{domain}/{host}/tmp
env[TMPDIR] = /home/http/{domain}/{host}/tmp
env[TEMP] = /home/http/{domain}/{host}/tmp

; host-specific php ini settings here
; php_admin_value[open_basedir] = /home/http/{domain}/{host}/www:/home/http/{domain}/{host}/tmp
"""

    print(args.domain, args.host, args.user, args.engine)

def delvhost(args):
    pass

def listvhost(args):
    pattern = ''
    if args.p:
        pattern = args.p

    if args.status == 'enable':
        abspath = nginx_enable_path
    else:
        abspath = nginx_available_path

    fn = sorted(glob.glob('%s/%s*' % (abspath, pattern)))

    for vhostfile in fn:
        vhost = path.basename(vhostfile)
        if vhost=='default' or vhost=='00-default':
            pass
        elif args.status == 'disable' and is_enable(vhost):
            pass
        else:
            print_status(vhost)

# Enable a vhost
def enable(args):
    if is_enable(args.vhost):
        print("Nothing to do !")
    elif path.exists(get_path_available(args.vhost)) == False:
        print("Vhost not created. Cannot Enable ...")
    else:
        link(get_path_available(args.vhost), get_path_enable(args.vhost))
        print("Vhost succesfully enabled !")

# Disable a vhost
def disable(args):
    if is_enable(args.vhost):
        unlink(get_path_enable(args.vhost))
        print("Vhost successfully disabled !")
    else:
        print('Vhost Not enable ! Nothing to do')

# parse argument group
def parse(args):
    p = argparse.ArgumentParser(prog='mkvhost')
    sp = p.add_subparsers()

    # new command
    sp_new = sp.add_parser('new', help='new help')
    sp_new.add_argument('domain', help='Ex: vudunet.net')
    sp_new.add_argument('host', help='Ex: www')
    sp_new.add_argument('user', help='Ex: vudunet')
    sp_new.add_argument('engine', choices={'none','php','python'}, metavar='engine')
    sp_new.set_defaults(func=mkvhost)

    # delete command
    sp_del = sp.add_parser('delete')
    sp_del.add_argument('domain', help='Ex: vudunet.net')
    sp_del.add_argument('host', help='Ex: www')
    sp_del.set_defaults(func=delvhost)

    # list command
    sp_list = sp.add_parser('list', help='list help')
    sp_list.add_argument('status', choices={'all','enable','disable'}, metavar='status')
    sp_list.add_argument('-p', required=False, help='filter result with pattern')
    sp_list.set_defaults(func=listvhost)

    # enable command
    sp_enbl = sp.add_parser('enable', help='enable help')
    sp_enbl.add_argument('vhost', help='Ex: www.vudunet.net')
    sp_enbl.set_defaults(func=enable)

    # disable command
    sp_dsbl = sp.add_parser('disable', help='disable help')
    sp_dsbl.add_argument('vhost', help='Ex: www.vudunet.net')
    sp_dsbl.set_defaults(func=disable)
        
    args = p.parse_args()
    try:
        a = getattr(args, "func")
    except AttributeError:
        p.print_help()
        sys.exit(0)

    args.func(args)

init_pkg()
print(sys.path)
#parse(sys.argv)
