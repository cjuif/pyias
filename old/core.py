""" Core Lib file. Contains all basic function to make it work """
import re
import glob

from sys import exit
from os import path
from os import link, unlink
from iasconf import setup
from iaslib import term



def get_path_enable(vhost):
    return path.join(setup.nginx_enable_path, vhost)

def get_path_available(vhost):
    return path.join(setup.nginx_available_path, vhost)

def is_vhost_enable(vhost):
    if path.exists(get_path_enable(vhost)):
        return True
    return False

def get_vhost_status(vhost):
    if is_vhost_enable(vhost):
        print('%s %s' % (term.white(vhost),term.side(term.green('enable'))))
    else:
        print('%s %s' % (term.white(vhost),term.side(term.red('disable'))))

def is_domain_valid(domain):
    pattern = "(?=^.{4,255}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63}$)"
    ndd = re.match(pattern, domain)
    if ndd:
        return True
    return False

def is_host_valid(host):
    pattern = "(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-))$)"
    host_name = re.match(pattern, host)
    if host_name:
        return True
    return False

def is_user_valid(user):
    pattern = "([a-z_][a-z0-9_]{0,30})" 
    u = re.match(pattern, user)
    if u:
        return True
    return False

def check_vhost_arg(domain, host, user):
    if not is_domain_valid(domain):
        sys_exit("%s is not a valid domain" % domain)
    
    if not is_host_valid(host):
        sys_exit("%s is not a valid host" % host)
    
    if not is_user_valid(user):
        sys_exit("%s is not a valid username" % user)
        
    return (domain, host, user)

def mkvhost(args):
    # Verification des entrees (domain host user
    # Creation Rep /home/http/domain/host/{www/htdocs,socks,tmp}
    # Prompt du password
    # Creation user insert sql
    # Creation fichier nginx

    v = check_vhost_arg(args.domain, args.host, args.user)
    vhost = v[1] + '.' + v[0]
    engine = args.engine
    index_file = setup.nginx_index_file
    nginx_engine_tpl = setup.nginx_tpl
    
    # Render Nginx tpl
    # if php render php tpl
    # if python render python tpl
    # if engine not "none"
    #    prompt for password
    #    create bdd
    #    create bdd user
    #    give right to user on bdd
    #    create sys user
    #    create directory tree
    #    change right on directory tree

    if engine == "php":
        nginx_engine_tpl = setup.nginx_tpl + setup.nginx_php_tpl
        index_file = setup.php_index_file
    
    nginx_final_tpl = nginx_engine_tpl + setup.nginx_close_tpl

    nginx_context = {
        "vhost":vhost,
        "docroot":setup.nginx_doc_root,
        "domain":domain,
        "host":host,
        "webdir":setup.nginx_web_dir,
        "indexfile":index_file,
        "security":setup.nginx_security_file,
        "fastcgi":setup.nginx_fastcgi_file,
        "socketfile":setup.php_socks_file
    }
    with open(get_path_available(vhost), 'w') as vhostfile:
        vhostfile.write(nginx_final_tpl.format(**nginx_context))

    #log = get_password()
    #print(log)

def delvhost(args):
    # Delete Nginx conf
    disablevhost(args.vhost)
    if path.exists(get_path_available(args.vhost)):
        unlink(get_path_available(args.vhost))
    else:
        sys_exit("Vhost does not exist. Cannot delete ...")

def listvhost(args):
    pattern = ''
    if args.p:
        pattern = args.p

    if args.status == 'enable':
        abspath = setup.nginx_enable_path
    else:
        abspath = setup.nginx_available_path

    fn = sorted(glob.glob('%s/%s*' % (abspath, pattern)))

    for vhostfile in fn:
        vhost = path.basename(vhostfile)
        if vhost=='default' or vhost=='00-default':
            pass
        elif args.status == 'disable' and is_vhost_enable(vhost):
            pass
        else:
            get_vhost_status(vhost)

def enablevhost(args):
    if is_vhost_enable(args.vhost):
        print("Nothing to do !")
    elif path.exists(get_path_available(args.vhost)) == False:
        sys_exit("Vhost not created. Cannot Enable ...")
    else:
        link(get_path_available(args.vhost), get_path_enable(args.vhost))
        print("Vhost succesfully enabled !")

def disablevhost(args):
    if is_vhost_enable(args.vhost):
        unlink(get_path_enable(args.vhost))
        print("Vhost successfully disabled !")
    else:
        print('Vhost Not enable ! Nothing to do')
    
def enablephp(args):
    pass

