# Description #

Ce script fournit une méthode pour automatiser le déploiement d'un serveur Nginx, php-fpm, mysql, varnish sur des serveurs dédiés fonctionnant avec la distribution Gentoo.

### **Comment faire ?** ###

* Prérequis.

Vous devez avoir un serveur (VM ou physique) installé avec Gentoo conformément au [Gentoo Handbook](http://www.gentoo.org/doc/en/handbook/handbook-amd64.xml)

* Installation

Lancer setup.py avec 

```
#!bash

./setup.py
```

* Configuration
Ajustez le fichier iasconf/setup.py à vos besoins.

# TODO #
Finalisation des scripts mkvhost, mkftp, mkbdd, mkbackup
